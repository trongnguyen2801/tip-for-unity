
### Zoe Nguyen Set Up IAP For Android Unity


1. Step 1: Create IAP in Google Play Console
    - Create IAP in app and set price (set string get like to set in code type: com.companyname.nameProducts) 

2. Step 2: Install IAP in Asset Pakage Unity
        - Step by Step in tutorial of pakage
        - Set Legacy Analytics in project setting to ON

3. Step 3: Use Button IAP and assign string iap you set in Step 1

4. Step 4: Coding to implement

```
public class IAPController : MonoBehaviour
{

    private string gem100 = "com.kongsoftware.iapkick.gem100";

    [SerializeField]
    private bool UseFakeStore = true;

    private void Awake()
    {
            
        #if UNITY_ANDROID
                ConfigurationBuilder builder = ConfigurationBuilder.Instance(
                    StandardPurchasingModule.Instance(AppStore.GooglePlay)
                );
        #elif UNITY_IOS
                ConfigurationBuilder builder = ConfigurationBuilder.Instance(
                    StandardPurchasingModule.Instance(AppStore.AppleAppStore)
                );
        #else
                ConfigurationBuilder builder = ConfigurationBuilder.Instance(
                    StandardPurchasingModule.Instance(AppStore.NotSpecified)
                );
        #endif
        
        if (UseFakeStore) // Use bool in editor to control fake store behavior.
        {

            StandardPurchasingModule.Instance().useFakeStoreUIMode = FakeStoreUIMode.StandardUser; // Comment out this line if you are building the game for publishing.
            StandardPurchasingModule.Instance().useFakeStoreAlways = true; // Comment out this line if you are building the game for publishing.
        }
    }

    public void OnPurchaseComplete(Product product)
    {
        //TODO Trong Add Gem if complete buy

        Debug.Log("TRONGBUY : OnCompleteBuyGem");
        
        if (product.definition.id == gem100)
        {
            FinancePreferConfig.AddSubGemsBudget(100);
        }
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureDescription purchaseFailureReason)
    {

        Debug.Log(product.definition.id + " failure reason  " + purchaseFailureReason);
        PopupStore.instance.SetActiveError();
        Debug.Log("TRONG Error: On Purchase Failed");
    }
}
```