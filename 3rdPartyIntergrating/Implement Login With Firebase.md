
### Zoe Nguyen Set Up Firebase Login For Unity (Should using Unity version 2022 or later and jdk should using version 11 to avoid getting error grandle)

##### The document are written base on https://www.youtube.com/watch?v=XVZwX8ojW-E , https://www.youtube.com/watch?v=zuA0vAg2L2k

##### If you have outside error in this file you can follow the link : https://github.com/firebase/quickstart-unity/issues


##### Error

- Khi đọc logcat thấy thông báo lỗi `authentication failed developer error`: kiểm tra config `web app client id` ở bước 6 đã đúng chưa. Tham khao them https://answers.unity.com/questions/1357666/google-play-games-services-authentication-fails.html

- "Chạy thêm cái google() trong dependency của gradle với activate cái androidx xong cài thêm androidx trong plugins. Cái thiếu chính là install package nó ko cài androidx cho mình và cũng ko báo lỗi chỗ đó."

##### Start

1. Step 1: Download and Import GPGP SDK

    > link: https://github.com/playgameservices/play-games-plugin-for-unity/tree/master/current-build

    - If you get error:  conflict dependence you should go to folder in unity: `GooglePlayGame > com.google.playgame > Editor > GooglePlayGamePluginDependency`
         -> replace line repository with:
                        `Assets/GooglePlayGames/com.google.play.games/Editor/m2repository`

2. Step 2: Create and Setup Firebase Project

    - Choose register app for apple or android

    - Type package name you set in unity project setting to link PJ (type: com.company.namepakage)

    - Type name project firebase you want set name 

    - Download google-services.json and trans it to unity asset (if you have any change in firebase project you must download again and replace it in unity asset)

    - Download and Import Firebase SDK (The reccomended order is Auth, Analyze, Database)
    
    > link: in page when you setup project or you can down in homepage firebase (reccomended version 11.6) 

3. Step 3: Configure Unity setting

    - In tab Player change Scripping Backend To IL2CPP

    - SDK lowest reccomended 26 or later, SDK Higest is Automatic or 33,34

    - Package name is the same package you set in firebase package to link each other when running

4. Step 4: Set up Google Play Console

    - Create app (tick all declerations)

    ![alt text](Images/createfirebase.PNG "Title Text")

    - Click Configtion in tab in left hand 

    - Choose yes, my app already in Google APIs -> choose app you create in firebase

    - In page when you click OK, scroll to Credential click `Add Creadential in Authentication > Create OAuth > Go to Google Cloud setup`
    ![alt text](Images/creadential1.PNG "Title Text")


5. Step 5: Set up Google Cloud AuthO

    - CLick tab `Credential in tab left hand > Create OAuth client ID > Continue Consent Screen`

    - User type is External to public for everyone can access to Login
    
    - Save and Continue

    - Save and Continue

    - Save and Continue

    - Click to `Credential > Create Creadential > Oauth client ID`
    
    ![alt text](Images/credential3.PNG "Title Text")
    -
    ![alt text](Images/credential2.PNG "Title Text")

    - Type package name like unity setting and firebase setting

    - Copy keystone in asset to `C:/Program FIle x86/Java/jre.../bin/ `

    - Open Terminal and cd to path `C:\Program FIle x86\Java\jre...\bin\`  (replace path of you)

    - Run:
        `keytool -keystore path-to-debug-or-production-keystore -list -v`  (replace path-to-debug-or-production-keystore to path keystore follow the path above)

        ![alt text](Images/sha.PNG "Title Text")

    = Type password keystore

    - Copy SHA and paste to SHA fingersprint in Google Cloud in above
    
    - Create

    - Go to setting firebase paste SHA and save
    ![alt text](Images/shafirebase.PNG "Title Text")

6. Step 6: Go to tab google play console before

    - Add OAuth client choose Auth you just create a monment ago
    
    - Save changes

    - Add Creadential again but this time choose Game Server in type and Authorisation choose server Auth you just create a monment ago

    - Check client ID the same it in google cloud , show it in `google cloud > Oauth 2.0 client IDs > click to download in Actions and check and Copy ClientID Server , ClientSecret`

    - Save changes in GGPlayServices

    - Get Resource in tab Authorisation before (next to button Create Creadential)

    - Copy all XML Android

    - In unity click `Windows > Google Play Services > Setup > Android Setup > Paste XML > Paste ClientID Server in tab under -> Setup`

    - `Windows > Google Play Services > Setup > Nearby Connect > Paste pakage name in tab > Setup`

7. Step 7: Configtion Authentication In Firebase

    - `Sign-in method > Play Games > Enable > Paste ClientID Server in tab ClientID, Client Secret in ClientSecret`

    ![alt text](Images/authenticateggpl.PNG "Title Text")

    - Setup Code To Login

8. Step 8: Configtion Project Setting When Build App

    (You must choose Grandle Properties before you choose Base Grandle to avoid error)

    - Choose the Main Grandle

    - Choose the Launcher Grandle

    - Choose Base Grandle

    - Choose Grandle Properties
